# AlarmClock
This is a simple desktop alarm clock application.<br>
It's a limited implementation, but there are more things to add.<br>
<br><br>
KNOWN LIMITATIONS:
<br>-No user settings (so far).
<br>-Users can't select multiple days
<br><br>
TO DO:<br>
<br>-Proper Javadocs (+++)
<br>-Error checking (+++)
<br>-Look and feel (+)
