/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.alarmlogic;

import com.alarmclock.utility.CustomProperties;

import java.io.*;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AlarmProperties {

    private static final Logger debugLog = Logger.getLogger(AlarmProperties.class.getSimpleName());

    private static Properties properties = new Properties();
    //CustomProperties: Modified Properties class that removes the date time added on file creation.
    //No need for the class to be committed to the project.
    private static CustomProperties userSettings = new CustomProperties();

    public static void loadAppProperties() {
        debugLog.log(Level.INFO, "Loading application properties...");
        try {
            properties.load(AlarmProperties.class.getClassLoader().getResourceAsStream("config.properties"));
        } catch (IOException e) {
            debugLog.log(Level.SEVERE, "Cannot load app properties: " + e.getMessage());
        }
    }

    public static void loadUserProperties() {
        debugLog.log(Level.INFO, "Loading user settings...");
        StringBuilder alarmSettingsLoc = new StringBuilder();
        alarmSettingsLoc.append(System.getProperty("user.dir"))
                .append("/")
                .append(properties.getProperty("USER_SETTINGS_LOCATION"))
                .append("/")
                .append(properties.getProperty("USER_SETTINGS_FILENAME"));

        File userSettingsFile = new File(alarmSettingsLoc.toString());

        if (createSettingsDirectory().isPresent())
            debugLog.log(Level.INFO, "Settings directory created.");
        else
            debugLog.log(Level.INFO, "Settings directory exists.");

        try (FileOutputStream userSettingsOStream = new FileOutputStream(userSettingsFile, true)) {
            if (!checkFileIsEmpty(userSettingsFile)) {
                try (FileInputStream userSettingsIStream = new FileInputStream(userSettingsFile)) {
                    if (userSettingsIStream != null)
                        userSettings.load(userSettingsIStream);
                    else
                        debugLog.log(Level.SEVERE, "No valid input stream.");
                }
            } else {
                loadDefaultUserSettings();
                //TODO Add proper comment to settings.cfg
                userSettings.store(userSettingsOStream, "");
            }
        } catch (FileNotFoundException e) {
            debugLog.log(Level.SEVERE, "No valid file found: " + e.getMessage());
        } catch (IOException e) {
            debugLog.log(Level.SEVERE, "Cannot load user properties: " + e.getMessage());
        }
    }

    private static Optional createSettingsDirectory() {
        File settingsDir = new File(getProperty("USER_SETTINGS_LOCATION"));

        if (!settingsDir.exists()) {
            return Optional.of(settingsDir.mkdir());
        } else
            return Optional.empty();
    }


    private static void loadDefaultUserSettings() {
        //TODO Proper user settings
    }

    private static boolean checkFileIsEmpty(File file) throws IOException {
        try (BufferedReader bufferReader = new BufferedReader(new FileReader(file))) {
            return bufferReader.readLine() == null;
        }
    }

    public static String getUserSetting(String reqProp) {
        return userSettings.getProperty(reqProp);
    }

    public static String getProperty(String reqProp) {
        return properties.getProperty(reqProp);
    }

}
