/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.alarmlogic;

import com.alarmclock.sound.AlarmSong;
import com.alarmclock.ui.AlarmView;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Map;
import java.util.Objects;

/**
 * Threaded class that runs alarm checking logic
 */
public class AlarmCheckThread extends Thread {

    private static final int MINUTE_IN_SECONDS = 60;
    private static final int SECOND_IN_MS = 1000;
    private boolean alarmActive = false;
    private String currentActiveAlarmID;

    /**
     * Checks every alarm data if it has to be fired
     */
    private void alarmCheckLoop() {
        for (Map.Entry<String, AlarmDataModel> data : AlarmController.getController().getModelMap().entrySet()) {
            if (
                    data.getValue().getDayOfTheWeek() == LocalDate.now().getDayOfWeek().getValue() &&
                            data.getValue().getMinute() == LocalTime.now().getMinute() &&
                            data.getValue().getHour() == LocalTime.now().getHour() &&
                            AlarmController.getController().getModelMap().get(data.getKey()).isActive()
                    ) {
                currentActiveAlarmID = data.getKey();
                alarmActive = !alarmActive;
                break;
            }
        }
    }

    private synchronized boolean checkAlarmSong() throws InterruptedException {
        while (AlarmSong.getSong().isPlaying()) {
            wait(1000);
        }
        return true;
    }

    /**
     * Fires alarm determined by alarmCheckLoop()
     */
    private void fireAlarmView() throws NullPointerException {
        for (Map.Entry<String, AlarmView> view : AlarmController.getController().getViewMap().entrySet()) {
            if (Objects.equals(view.getKey(), currentActiveAlarmID) && alarmActive) {
                view.getValue().setMuteState(true);
                assert false;
                AlarmSong.getSong().setSong(
                        getClass().getClassLoader().getResource(
                                "songs/" +
                                        AlarmProperties.getProperty("DEFAULT")).toString());
                AlarmSong.getSong().play();
                view.getValue().setMuteState(true);
                try {
                    checkAlarmSong();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    view.getValue().setMuteState(false);
                    AlarmController.getController().startNewAlarmThread();
                }
                break;
            }
        }
    }

    /**
     * At the start of the thread it waits [60 seconds - current second] before it starts checking the alarms
     * Only need to check alarms every minute if they should be fired
     * If any alarm is active, it fires
     * Only one alarm can fire simultaneously
     */
    @Override
    public void run() {
        try {
            sleep((MINUTE_IN_SECONDS - LocalTime.now().getSecond()) * SECOND_IN_MS);
            while (!alarmActive) {
                alarmCheckLoop();
                if (!alarmActive) sleep(MINUTE_IN_SECONDS * SECOND_IN_MS);
            }
            fireAlarmView();
        } catch (InterruptedException | NullPointerException e) {
            e.printStackTrace();
        }
    }
}
