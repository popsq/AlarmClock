/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.ui;

import javax.swing.*;
import java.awt.*;

/**
 * The type GUI base.
 */
public class FrameView extends JFrame {

    private static final FrameView INSTANCE = new FrameView();
    private final SpringLayout defaultLayout = new SpringLayout();

    /**
     * Instantiates a new GUI base.
     */
    private FrameView() {
        setFrameSettings();
    }

    public static FrameView getInstance() {
        return INSTANCE;
    }

    public void addComponent(Component com) {
        this.getContentPane().add(com);
    }

    private void setFrameSettings() {
        getContentPane().setLayout(defaultLayout);
        //setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}