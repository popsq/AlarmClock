/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.ui;

import com.alarmclock.alarmlogic.AlarmController;
import com.alarmclock.alarmlogic.AlarmProperties;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import java.awt.*;
import java.util.Objects;

public class AlarmView extends JComponent {

    private final Border defaultBorder = new BevelBorder(BevelBorder.RAISED);
    private final GridBagLayout defaultAlarmLayout = new GridBagLayout();
    private final GridBagConstraints defaultConstraints = new GridBagConstraints();
    private AlarmComboBoxView alarmDataMinutes;
    private AlarmComboBoxView alarmDataHours;
    private AlarmComboBoxView alarmDataDayOfTheWeek;

    private AlarmButtonView muteAlarmButtonView = new AlarmButtonView(AlarmButtonViewType.MUTE);
    private AlarmButtonView removeAlarmButtonView = new AlarmButtonView(AlarmButtonViewType.REMOVE);
    private AlarmButtonView editAlarmButtonView = new AlarmButtonView(AlarmButtonViewType.EDIT);

    /**
     * Instantiates a new Alarm.
     *
     * @param alarmSize the alarm size
     */
    public AlarmView(Dimension alarmSize) {
        setAlarmViewButtons();
        addAlarmViewComponents();
        setAlarmSettings(alarmSize);
    }

    public void setAlarmViewButtons() {
        alarmDataMinutes = new AlarmComboBoxView(AlarmDataViewType.MINUTES);
        alarmDataHours = new AlarmComboBoxView(AlarmDataViewType.HOURS);
        alarmDataDayOfTheWeek = new AlarmComboBoxView(AlarmDataViewType.DAYOFTHEWEEK);

        AlarmController.getController().defineButton(muteAlarmButtonView, muteAlarmButtonView.getType());
        AlarmController.getController().defineButton(editAlarmButtonView, editAlarmButtonView.getType());
        AlarmController.getController().defineButton(removeAlarmButtonView, removeAlarmButtonView.getType());
    }

    private void addAlarmViewComponents() {
        add(editAlarmButtonView, defaultConstraints);
        add(muteAlarmButtonView, defaultConstraints);
        add(alarmDataDayOfTheWeek, defaultConstraints);
        add(alarmDataHours, defaultConstraints);
        add(alarmDataMinutes, defaultConstraints);
        add(removeAlarmButtonView, defaultConstraints);
    }

    public void setEditable(Boolean visibility) {
        if (visibility) {
            editAlarmButtonView.setText(AlarmProperties.getProperty("ALARM_ON_TEXT"));
        } else {
            editAlarmButtonView.setText(AlarmProperties.getProperty("ALARM_OFF_TEXT"));
        }

        editAlarmButtonView.setPressed(!visibility);

        alarmDataDayOfTheWeek.setEnabled(visibility);
        alarmDataMinutes.setEnabled(visibility);
        alarmDataHours.setEnabled(visibility);
    }

    @Override
    public int hashCode() {

        int result = alarmDataMinutes.hashCode();
        result = 31 * result + alarmDataHours.hashCode();
        result = 31 * result + alarmDataDayOfTheWeek.hashCode();
        result = 31 * result + muteAlarmButtonView.hashCode();
        result = 31 * result + removeAlarmButtonView.hashCode();
        result = 31 * result + editAlarmButtonView.hashCode();
        return result;
    }

    private void setAlarmSettings(Dimension size) {
        setBorder(defaultBorder);
        setLayout(defaultAlarmLayout);
        setMinimumSize(size);
        setMaximumSize(size);
        setPreferredSize(size);
        setVisible(true);
    }

    public void activateComboBoxes() {
        alarmDataHours.activateComboBox(this);
        alarmDataMinutes.activateComboBox(this);
        alarmDataDayOfTheWeek.activateComboBox(this);
    }

    public String getViewKey() {
        return AlarmController.getController().getViewMap().entrySet()
                .stream()
                .filter(entry -> entry.getValue().equals(this))
                .findAny()
                .get().getKey();
    }

    public void setMuteState(boolean flag) {
        muteAlarmButtonView.setEnabled(flag);
    }

    @Override
    public String toString() {
        if (Objects.equals(getViewKey(), "")) return "Invalid View.";
        return new StringBuilder().append("View for Alarm ").append(getViewKey()).toString();
    }
}
