/*
 *
 *  *     Created by Popescu Andrei Bogdan.
 *  *     GitHub: https://github.com/andreibogdanpopescu
 *  *
 *  *     This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.alarmclock.sound;

import com.alarmclock.utility.MediaPlayerI;
import javafx.scene.media.AudioClip;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AlarmSong implements MediaPlayerI<String> {

    private static final Logger debugLog = Logger.getLogger(AlarmSong.class.getSimpleName());

    private static final AlarmSong INSTANCE = new AlarmSong();
    private static AudioClip song = null;

    private AlarmSong() {
    }

    public static AlarmSong getSong() {
        return INSTANCE;
    }

    @Override
    public void setSong(String path) {
        debugLog.log(Level.INFO, "Song path: " + path);
        song = new AudioClip(path);
    }

    @Override
    public boolean isRepeat() {
        return song.getCycleCount() > 1;
    }

    @Override
    public boolean isPlaying() {
        return song.isPlaying();
    }

    @Override
    public void play() {
        try {
            song.play();
        } catch (NullPointerException e) {
            debugLog.log(Level.SEVERE, "No song to play! Check if you have set the song path before starting.");
        }
    }

    @Override
    public void stop() {
        try {
            song.stop();
        } catch (NullPointerException e) {
            debugLog.log(Level.SEVERE, "No song to stop! Check if you have set the song path before starting.");
        }
    }
}
